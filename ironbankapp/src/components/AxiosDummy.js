
import { transactions } from './dummyTransactions'

class Axios {

    constructor() {
        this.dummyTrue = true
    }

    get = (path,header) => {

        if(path.search("/token") > 0 ){
            return new Promise((resolve, reject) => {
        
                setTimeout(function(){
                    //auth url: 7gq3bccyoa.execute-api.eu-west-1.amazonaws.com/v1
                    console.log("resolve dummmy true")
                    resolve({data: this.dummyTrue}); 
                }, 550);
            })
        }else{
            // transaction request
            return new Promise((resolve, reject) => {

                setTimeout(function(){
                    //auth url: 7gq3bccyoa.execute-api.eu-west-1.amazonaws.com/v1
                    console.log("resolve dummmy transaction")
                    resolve({data: transactions}); 
                }, 550);
            })
        }
    }
}
export default Axios
