import React, { Component } from 'react';
import Axios from './AxiosDummy';
import { withRouter } from 'react-router-dom';
import { getJwt, createHeaders } from '../helpers';

class AuthComponent extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false
    };
    this.token = "mytokenstringthatneedstobegenerated"
    this.key = "7bNIkDyqJ75Qh9pSgcUbUa3epCLN90OK4ZO3dA2H"
    this.partner = "95d3ca6606eb44b3a9d9f8796bfb55da" 
    this.axios = new Axios()
    
  }

  componentDidMount() {
    
    // check if we have a local JWT Token
    const jwt = getJwt();
    
    if (!jwt) {
      this.setState({
        isAuthenticated: false
      });
      return;
    }
    
    // check if we have a token 
    this.axios.get('/token', { headers: createHeaders(this.token, this.key, this.partner) }).then(res => {
      
      this.setState({
        isAuthenticated: true // true for now in dummy
      })
    });
  }

  render() {
    const { isAuthenticated } = this.state;
    
    if (isAuthenticated === false) {
      return (
        <div>
          Loading...!
        </div>
      );
    }

    if (isAuthenticated === true) {
      //this.props.history.push('/login');
    }

    return this.props.children;
  }
}

export default withRouter(AuthComponent);