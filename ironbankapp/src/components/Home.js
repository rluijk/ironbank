import React from 'react';
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div>
      Welcome to the IRON BANK HomePage<br></br>
      Go to "protected" area to see your  <Link to="/Transaction">Transactions</Link>
    </div>
  );
};

export default Home;