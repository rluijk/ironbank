import React, { useEffect, useState } from 'react';
import Axios from './AxiosDummy';
import {  createHeaders } from '../helpers';

const Transaction = () => {

    const [transactions,setTransactions] = useState([])

    // r7p2rhg4ji.execute-api.eu-west-1.amazonaws.com/v1
    // bankId and accountId dummies
    const token = "mytokenstringthatneedstobegenerated"
    const key = "7bNIkDyqJ75Qh9pSgcUbUa3epCLN90OK4ZO3dA2H"
    const partner = "95d3ca6606eb44b3a9d9f8796bfb55da" 
    const bankId = 123
    const accountId = 456

    useEffect( ()=> { 
        const axios = new Axios()
        axios.get(`/banking/${bankId}/accounts/${accountId}/transactions`,  
                { headers: createHeaders(token, key, partner) })
                .then( res => {
                    setTransactions(res.data.results)
                })
    }, []);

   // React keeps complaining on unique ids, but in browser tools, shows all are unique 
  return (
    <div>
        <table cellSpacing="10">
            <tbody>
                <tr>
                    <th> Transaction Id </th>
                    <th> Amount</th>
                    <th> Reference</th>
                    <th> Company Id </th>
                    <th> Type</th>
                    <th> Status</th>
                </tr>
                {transactions.map((transaction,index) => 
                    <>
                        <tr key={index+transaction.transactionId+10}>
                            <td key={index+transaction.transactionId}>{transaction.transactionId}</td>
                            <td key={index+transaction.amount}>{transaction.amount}</td>
                            <td key={index+transaction.reference}>{transaction.reference}</td>
                            <td key={index+transaction.companyId}>{transaction.companyId}</td>
                            <td key={index+transaction.type}>{transaction.type}</td>
                            <td key={index+transaction.status}>{transaction.status}</td>
                        </tr>
                    </>    
                )}
            </tbody>
        </table>
    </div>
  );
};

export default Transaction;
