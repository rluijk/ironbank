
export const transactions = {
    "description": "Transactions response",
    "pageNo":	1,
    "results": [
            {
            "amount":	20000,
            "AccountId": "988399399-09930",
            "description": "Iron Mask for Prince",
            "merchant": "",
            "transactionSubCode": "abd-e2",
            "transactionCode"	: "2323",
            "type": "defense",
            "transactionId": "234",
            "reference": "Ironbank",
            "companyId": 3452,
            "bankId":	554,
            "proprietarySubCode": "erer4433",
            "bookingDate": "12-08-1845",
            "proprietaryCode": "IRNBNK",
            "currencyCode": "LTC",
            "status": "COMPLETE"
            },
            {
                "amount":	19700,
                "AccountId": "988399399-09930",
                "description": "Ransom Payment",
                "merchant": "",
                "transactionSubCode": "ran-psp",
                "transactionCode"	: "23223",
                "type": "ransom",
                "transactionId": "236",
                "reference": "Ironbank",
                "companyId": 3452,
                "bankId":	554,
                "proprietarySubCode": "ransD3",
                "bookingDate": "10-08-1845",
                "proprietaryCode": "IRNBNK",
                "currencyCode": "BTC",
                "status": "COMPLETE"
                }
    ],
    "noPages": 1
}