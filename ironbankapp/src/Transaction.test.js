import React from 'react';
import { expect } from 'chai';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Transaction from './components/Transaction';


Enzyme.configure({ adapter: new Adapter() });


describe('<Transaction />', () => {

  it('renders the Reference', () => {
    const wrapper = shallow(<Transaction />);
    expect(wrapper.text()).to.contain('Reference');
  });

});

