import React from 'react';
import { expect } from 'chai';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Home from './components/Home';


Enzyme.configure({ adapter: new Adapter() });


describe('<Home />', () => {

  it('renders the Bank Name: IRON BANK', () => {
    const wrapper = shallow(<Home />);
    expect(wrapper.text()).to.contain('IRON BANK');
  });
});