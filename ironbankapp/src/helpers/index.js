
import axios from 'axios';

export const getJwt = () => {
    return true
    //return 'Bearer ' + localStorage.getItem('dummy-jwt');
};

export const createHeaders = (token, key, partner) => {
    return {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'x-api-key': key,
        'x-partner-id': partner,
        'Authorization': `Bearer ${token}`
    }
}


export default function setAuthorizationToken(token,key,partner) {
  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    axios.defaults.headers.common['x-api-key'] = key;
    axios.defaults.headers.common['x-partner-id'] = partner;
  } else {
    delete axios.defaults.headers.common['Authorization'];
    delete axios.defaults.headers.common['x-api-key'];
    delete axios.defaults.headers.common['x-partner-id'];
  }
}