import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Home from './components/Home';
import AuthComponent from './components/AuthComponent';
import Transaction from './components/Transaction';
import './App.css';

function App() {

  return (
    <BrowserRouter>
    <div className="Iron-App">
      <Switch>
        <Route path={'/'} exact component={Home} />
        <AuthComponent>
          <Route path={'/Transaction'} component={Transaction} />
        </AuthComponent>
      </Switch>
    </div>  
    </BrowserRouter>
  );
}

export default App;
